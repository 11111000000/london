import { Next } from 'components/buttons'
import { LandingSlide } from 'components'
import { Typist } from 'components'

<LandingSlide>
<Typist>

# Year 2051

The greatest educational expedition in human history was launched from Moskovskiy cosmodrome.

</Typist>
<Next to='/2'>Next</Next>
</LandingSlide>
