---
back: "/2"
---

import { Next } from 'components/buttons'
import {Typist} from  'components'
import { Row, Col } from 'react-simple-flex-grid'
import "react-simple-flex-grid/lib/main.css"
import './map.css'

<div className="map-page">
<Row justify="center" style="text-align:center;">
<Col span={6}>

## To join the crew, go to the departure station and pass the registration

</Col>
</Row>
<Row justify="center">

### Departure time

</Row>

<Row gutter={10} align="middle" justify="space-between" className="departure-time">
<Col>

#### Wed 23 Jan
10:00 - 18:00

</Col>
<Col>

#### Thu 24 Jan
10:00 - 18:00

</Col>
<Col>

#### Fri 25 Jan
10:00 - 18:00

</Col>
<Col>

#### Sat 26 Jan
10:00 - 15:00

</Col>
</Row>
<Row>
<Col span="6" style="height:20em;">
</Col>
<Col span="6">

#### Coordinates of the space station

ExCeL London, Royal Victoria Dock
1 Western Gateway,
London E16 1XL,
United Kingdom

Bettshow
Stand «Moscow city department of education»
</Col>
</Row>
</div>
