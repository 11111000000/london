import { Next } from 'components/buttons'
import {Typist} from 'components'
import Helmet from 'react-helmet'
import { LandingSlide } from 'components'

<Helmet
  meta={[
  {
    property: `og:image`,
    content: `https://bett.api.coreapp.ai/uploads/olymp/share/6.jpg`,
  },
  {
    name: `twitter:title`,
    content: `I have participated in Quest #cityforedu`,
  },
  {
    name: `twitter:image:src`,
    content: `https://bett.api.coreapp.ai/uploads/olymp/share/6.jpg`,
  },
  {
    name: `twitter:image`,
    content: `https://bett.api.coreapp.ai/uploads/olymp/share/6.jpg`,
  }
]} />


<LandingSlide>
<Typist>

# Year 2051

The greatest educational expedition in human history was launched from Moskovskiy cosmodrome.

</Typist>

<Next to='/2'>Next</Next>
</LandingSlide>
