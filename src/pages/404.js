import React from 'react'

import Layout from 'components/layout'
import SEO from 'components/seo'

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <h1>Ничегошеньки</h1>
    <p>Тут пока ничего, добавь роут.</p>
  </Layout>
)

export default NotFoundPage
