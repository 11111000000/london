import React from 'react'
import styled from 'styled-components'
import Video from 'react-background-video-player'
import videoSrc from 'assets/360.mp4'
import windowDimensions from 'react-window-dimensions'

const BackgroundVideo = ({width, height, ...props }) => (

  (typeof window !== 'undefined') ? (

    <Video {...props}
           containerHeight={height}
           containerWidth={width}
           src={videoSrc}
           autoPlay={true}
    />) : <div />)

export default ((typeof window !== 'undefined') ?

                windowDimensions() : ( a => a ))(

  styled( BackgroundVideo )(`
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    zIndex: 0;
`))
