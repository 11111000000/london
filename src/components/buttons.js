import React from 'react'
import Link from './link'
import styled from 'styled-components'
import ArrowSvg from 'assets/left.svg'

export const ReadyButton = styled(Link)`
  display: flex;
  flex: 0 0 auto;
  justify-content: center;
  align-items: center;
  padding: 1em 1em;
  box-sizing: border-box;
  border-radius: 4px;
  box-shadow: 0 6px 22px -4px rgba(185, 0, 255, 0.62);
  background-color: #7b00aa;
  text-align: center;
  color: #ffffff;
  box-sizing: border-box;
  font-family: ModernHeadFont;
  text-decoration: none;

  &:hover {
    background-color: #6c0096;
    box-shadow: none;
  }

  &:active {
    box-shadow: inset 0 1px 15px 0 rgba(0, 0, 0, 0.5);
    background-color: #6c0096;
    border-radius: 4px;
  }
`

export const Ready = ReadyButton  // alias

// export const fromStand = () =>
//   ((typeof window !== 'undefined')
//    &&
//    window.localStorage.getItem('stand') !== "false")

// export const NextOnBoard = props =>
//   <Next to={ fromStand() ? '/3' : '/map' } >
//     Next
//   </Next>

export const NextOnBoard = props =>
  <Next to="/3">
    Next
  </Next>


export const NextButton = styled(Link)`
  display: flex;
  flex: 0 0 auto;
  justify-content: center;
  align-items: center;
  padding: .7em 2.7em;
  border: 2px solid #bd48ff;
  text-transform: lowercase;
  box-sizing: border-box;
  border-radius: 4px;
  background-color: transparent;
  text-align: center;
  color: #bd48ff;
  box-sizing: border-box;
  font-family: ModernHeadFont;
  text-decoration: none;

  &:hover {
    background-color: #7b00aa;
    box-shadow: none;
    color: #000000;
    border: 2px solid #7b00aa;
  }

  &:active {
    box-shadow: 0 6px 22px -4px rgba(185, 0, 255, 0.62);
    background-color: #7b00aa;
    color: #000000;
    border: 2px solid #7b00aa;
  }
`

export const Next = NextButton  // alias for NextButton

export const BackButton = styled(
  (props) => (
     <Link {...props}>
      <ArrowSvg/> Back
     </Link>)
)`
  display: flex;
  flex: 0 1 auto;
  align-self: center;
  font-weight: bold;
  text-align: center;
  color: #ffffff;
  text-decoration: none;
  font-family: ModernHeadFont;
  font-size: 1.2em;
  cursor: pointer;

  svg {
    display: flex;
    flex: 1 0 auto;
    margin: auto;
    width: 1em;
    height: 1em;
    margin-right: .4em;
    margin-bottom: .2em;
  }

  &:hover {
    color: #7b00aa;
    svg path {
      fill: #7b00aa;
    }
  }

  &:active {
     color: #6c0096;
  }
`
