import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'
//import BackgroundVideo from './video'
import BackgroundGalaxy from './galaxy'
import styled from 'styled-components'
//import media from 'styled-media-query'
import SuperQuery from '@themgoncalves/super-query';
import SEO from './seo'

import { createGlobalStyle } from 'styled-components'

import 'normalize.css'
import './fonts.css'

const breakpoints = {
    xs: 0,
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1200,
};

const media = SuperQuery(breakpoints)

const GlobalStyle = createGlobalStyle`
  html {
    font-family: ModernHeadFont,Helvetica, Arial, sans-serif;
    -webkit-font-smoothing: antialiased;
    font-feature-settings: 'calt', 'liga', 'hist', 'onum', 'pnum';
    overflow: hidden;
    color: #fff;
    background-color: #000;
    font-size: 1rem;

    ${media.all
      .and.minWidth.xs
      .css` font-size: .6rem; `};

    ${media.all
      .and.minWidth.xs
      .and.minResolution.of('120dpi')
      .css` font-size: .6rem; `};

    ${media.all
      .and.minWidth.xs
      .and.minResolution.of('120dpi')
      .and.landscape
      .css` font-size: .6rem; `};

    ${media.all
      .and.minWidth.sm
      .css` font-size: .9rem; `};

    ${media.all
      .and.minWidth.sm
      .and.landscape
      .css` font-size: .8rem; `};

    ${media.all
      .and.minWidth.sm
      .and.minResolution.of('120dpi')
      .css` font-size: 1rem; `};

    ${media.all
      .and.minWidth.sm
      .and.minResolution.of('300dpi')
      .css` font-size: 1rem; `};

    ${media.all
      .and.minWidth.sm
      .and.minResolution.of('120dpi')
      .and.landscape
      .css` font-size: .7rem; `};

    ${media.all
      .and.minWidth.md
      .css` font-size: 1rem; `};

    ${media.all
      .and.minWidth.md
      .and.landscape
      .css` font-size: .9rem; `};

    ${media.all
      .and.minWidth.md
      .and.minResolution.of('120dpi')
      .css` font-size: 1.2rem; `};

    ${media.all
      .and.minWidth.md
      .and.minResolution.of('120dpi')
      .and.landscape
      .css` font-size: .7rem; `};

    ${media.all
      .and.minWidth.lg
      .css` font-size: 1rem; `};

    ${media.all
      .and.minWidth.lg
      .and.minResolution.of('120dpi')
      .css` font-size: 1.1rem; `};

    ${media.all
      .and.minWidth.lg
      .and.minResolution.of('300dpi')
      .and.landscape
      .css` font-size: 1.3rem; `};


    ${media.all
      .and.minWidth.xl
      .css` font-size: 1.3rem; `};

    ${media.all
      .and.minWidth.xl
      .and.minResolution.of('120dpi')
      .css` font-size: 1.5rem; `};

  }
`

const MainContainer = styled.div`
  display: block;
  position: absolute;
  z-index: 100;
  height: 100%;
  width: 100%;
  opacity: 0.9
  overflow: hidden;

`

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={ data => (
      <>
        <SEO />
        <GlobalStyle />
        <BackgroundGalaxy />
        <MainContainer>
          {children}
        </MainContainer>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
