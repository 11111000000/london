import React from 'react'
import styled from 'styled-components'
import { Grid, Row, Col } from 'react-styled-flexboxgrid'
import { Next, Ready } from 'components/buttons'

export const Typist = styled.div``

const TextContainer = styled.div`

  font-size: 1em;

  h1,h2 {
    font-family: ModernHeadFont;
    margin-top: 0;
  }

  h1 {
    font-size: 2.3em;
  }

  h2 {
    font-size: 1.5em;
  }

  h3 {
    font-family: ATCOverlook;
    font-size: 1.2em;
  }

  .Cursor {
    display: none;
  }

  ${ Next }, ${ Ready } {
    margin: auto;
    margin-bottom: 0;
    margin-top: auto;
  }
`

export const LandingSlide = styled(TextContainer)`
  display: flex;
  flex: 0 1 11em;
  align-self: center;
  flex-flow: column nowrap;
  max-width: 30em;
  margin: auto auto;
  text-align: center;
`

export const OnboardSlide = styled(TextContainer)`
  display: flex;
  flex: 1 1 100%;
  flex-flow: row nowrap;
  margin: 2em;
`

export const Left = styled.div`
  display: flex;
  flex: 0 0 40%;
  flex-flow: column nowrap;
  margin: auto 3%;
`

export const Right = styled.div`
  display: flex;
  flex: 0 0 55%;
  flex-flow: column nowrap;
  margin: auto 3%;
  text-align: left;

  ${Next}, ${Ready} {
    margin-top: 1em !important;
    margin-left: 0 !important;
  }
`

export default { Grid, Row, Col, Next, Ready }
