import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'

function SEO({ description, lang, meta, keywords, title }) {
  return (
    <StaticQuery
      query={detailsQuery}
      render={data => {

        const metaDescription =
          description || data.site.siteMetadata.description
        return (
          <Helmet
            htmlAttributes={{
              lang,
            }}
            title={title}
            titleTemplate={`%s | ${data.site.siteMetadata.title}`}
            meta={[
              {
                property: `og:title`,
                content: `I have participated in Quest #cityforedu`,
              },
              /* { */
              /*   property: `og:url`, */
              /*   content: `https://bett.go2019.ru`, */
              /* }, */
              {
                property: `og:image`,
                content: `https://bett.api.coreapp.ai/uploads/olymp/share/share.jpg`,
              },
              {
                name: `twitter:title`,
                content: `Try to pass  interactive quest "Moscow - Cassiopeia" in Bett show 2019`,
              },
              {
                name: `twitter:image:src`,
                content: `https://bett.api.coreapp.ai/uploads/olymp/share/share.jpg`,
              },
              {
                name: `twitter:image`,
                content: `https://bett.api.coreapp.ai/uploads/olymp/share/share.jpg`,
              }
            ]
              .concat(
                keywords.length > 0
                  ? {
                      name: `keywords`,
                      content: keywords.join(`, `),
                    }
                  : []
              )
              .concat(meta)}
          />
        )
      }}
    />
  )
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  keywords: [],
  title: ''
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.array,
  keywords: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string.isRequired,
}

export default SEO

const detailsQuery = graphql`
  query DefaultSEOQuery {
    site {
      siteMetadata {
        title
        description
        author
      }
    }
  }
`
