import React from 'react'
import styled from 'styled-components'
import CoreSvg from 'assets/powered.svg'
import Logo1Svg from 'assets/logo-1-1-x.svg'
import Logo2Svg from 'assets/logo-2-1-x.svg'
import { BackButton, NextButton, ReadyButton } from 'components/buttons'

const Container = styled.div`
  display: flex;
  flex: 0 0 100%;
  box-sizing: border-box;
  flex-direction: column;
  min-height: 100vh;
`

const Header = styled.div`
  display: flex;
  flex: 0 0 auto;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding-top: 2em;
`

const LogoStyle = `
  display: block;
  margin: auto 1em;
  width: auto;
  svg {
   height: 2.3em;
   width: auto;
  }
`

const HeaderLogo1 = styled(
  props =>
    <a {...props} href="https://go2019.ru/"><Logo1Svg /></a>)`${LogoStyle}`

const HeaderLogo2 = styled(
  props =>
    <a {...props} href="https://www.mos.ru/donm/"><Logo2Svg /></a>)`${LogoStyle}`

const Content = styled.div`
  display: flex;
  flex:  1 1 auto;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  font-family: ATCOverlook;
  font-size: 1.2em;
  padding: 0 1em;
  > div {
    display: flex;
    flex: 1 1 100%;
    flex-direction: column;
    margin: auto auto;
  }
`

const CoreLogo = styled(
  props =>
    <a {...props} href="https://corp.coreapp.ai/">
      <CoreSvg />
    </a>)`
  svg {
    display: block;
    width: auto;
    height: 3.1em;
  }

`

const Footer = styled.div`
  display: flex;
  flex: 0 0 auto;
  flex-flow: row nowrap;
  self-align: flex-end;
  margin-bottom: 0;
  justify-content: space-between;
  align-content: space-between;
  box-sizing: border-box;
  padding: .5em 2em 2em;
`

const Slide = (props,context) => {
  const {pageContext : { name, back }, children } = props
  // console.log(name)
  // if((typeof window !== 'undefined')) {
  //   if( !name || name == 'stand' ) {
  //     window.localStorage.setItem('stand', "true")
  //   } else if (['savedship','success','newplanet'].includes(name)) {
  //     window.localStorage.setItem('stand', "false")
  //   }
  // }


  return (
    <>
      {/* <SEO title="Slide" keywords={[`london`, `core`, `slide`]} /> */}
      <Container>
        <Header>
          <HeaderLogo1/>
          <HeaderLogo2/>
        </Header>
        <Content>
         { children }
        </Content>
        <Footer>
          { back ? <BackButton to={back} /> : <div/> }
          <CoreLogo />
        </Footer>
      </Container>
    </>
  )
}

export default Slide
