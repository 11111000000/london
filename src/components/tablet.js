import React from 'react'
import styled from 'styled-components'
import Img from 'gatsby-image'
import BackgroundImage from 'gatsby-background-image'
import { StaticQuery, graphql } from "gatsby"

const Cover = styled( (props) =>
                      <Img fadeIn={ false }
                           fluid={ props.data.tablet.childImageSharp.fluid }
                           {...props} />

                    )` display: block;
                       width: 100%;
                       height: auto;`

const Screen = styled( props =>
                     <div {...props}>
                       <Img fadeIn={ true }
                            fluid={ props.data[`screen${props.screen}`].childImageSharp.fluid } />
                     </div>
                     )` position: absolute;
                        top: 10%;
                        left: 12%;
                        bottom: 10%;
                        right: 12%;
                        overflow: hidden;`

const Tablet = styled(({data,screen,...props}) =>
                      <div {...props}>
                        <Cover data={data}>
                        </Cover>
                        <Screen data={data} screen={screen} />
                      </div>
                     )` position:relative; `

export default props => (
  <StaticQuery
    query ={
      graphql`
        fragment sharp on File {
          childImageSharp {
              fluid(maxWidth: 1024, quality: 100) {
                ...GatsbyImageSharpFluid_noBase64
                presentationWidth
              }
          }
        }
        fragment sharp2 on File {
          childImageSharp {
              fluid(maxWidth: 1024, quality: 100) {
                ...GatsbyImageSharpFluid
                presentationWidth
              }
          }
        }
        query {
          tabletFixed: file(relativePath: {eq: "i-pad-mini@2x.png"}) {
            childImageSharp {
              fixed {
                ...GatsbyImageSharpFixed
              }
            }
          }
          tablet: file(relativePath: {eq: "i-pad-mini@3x.png"}) {...sharp}
          screen1: file(relativePath: {eq: "screen-1.png"}) {...sharp2}
          screen2: file(relativePath: {eq: "screen-2.png"}) {...sharp2}
          screen3: file(relativePath: {eq: "screen-3.png"}) {...sharp2}
          screen4: file(relativePath: {eq: "screen-4.png"}) {...sharp2}
          screen5: file(relativePath: {eq: "screen-5.png"}) {...sharp2}
        }
      `}

    render={data => <Tablet data={data} {...props} />}

  />
)
