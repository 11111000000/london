import React from 'react'
import styled from 'styled-components'
import Img from 'gatsby-image'
import BackgroundImage from 'gatsby-background-image'
import { StaticQuery, graphql } from "gatsby"

const Container = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  z-index: 0;
`

const Space = styled( props =>
                      <BackgroundImage fluid={ props.image.childImageSharp.fluid } {...props} />
                    )` height: 100%; width: 100%; `


const Galaxy = (props) => {
  console.log(props)
  return (
    <Container className = {props.className}>
      <Space image = { props.space } />
    </Container>
  )}

export default props => (
  <StaticQuery
    query ={
      graphql`
        fragment somesharp on File {
          childImageSharp {
              fluid(maxWidth: 1200, quality: 100) {
                ...GatsbyImageSharpFluid
                presentationWidth
              }
          }
        }
        query {
          space: file(relativePath: {eq: "space01.jpeg"}) {...somesharp}
        }
      `}

    render={ props => <Galaxy {...props} />}

  />
)
