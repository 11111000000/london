const path = require(`path`)

// exports.onCreatePage = async ({ page, actions }) => {
//   const { createPage } = actions

//   if (page.path.match(/^\/slides/)) {
//     page.matchPath = "/slides/*"
//     createPage(page)
//   }
// }

exports.onCreateWebpackConfig = ({ stage, actions }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, "src"), "node_modules"],
    },
  })
}

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  const slideTemplate = path.resolve(`src/components/slide.js`)

  return new Promise((resolve, reject) => {
    resolve(
      graphql(
        `
          {
            allMdx(sort: { fields: fileAbsolutePath }) {
              edges {
                node {
                  frontmatter {
                    back
                  }
                  parent {
                    ... on File {
                      relativePath
                      absolutePath
                      name
                    }
                  }
                }
              }
            }
          }
        `
      ).then(result => {
        console.log(result)
        if (result.errors) {
          reject(result.errors);
        }

        createPage({
          path: `/`,
          component: result.data.allMdx.edges[0].node.parent.absolutePath,
          context: {},
        })

        result.data.allMdx.edges.forEach(({ node }) => {
          // console.log(`Creating page ${node.parent.relativePath}`)
          // console.log(node.parent)
          createPage({
            path: (`${node.parent.name}`),
            component: node.parent.absolutePath,
            context: { name: node.parent.name, back: node.frontmatter.back }
          })
        })
      })
    )
  })
}

  // return graphql(`
  //   {
  //     london {
  //       slides {
  //         markdown
  //         route
  //         button {
  //           text
  //           route
  //         }
  //       }
  //     }
  //   }
  // `).then(result => {

  //   if (result.errors) {
  //     throw result.errors
  //   }

  //   //console.log(result.data.london.slides)
  //   createPage({
  //     path: `/`,
  //     component: slideTemplate,
  //     context: {
  //       slide: result.data.london.slides[0]
  //     },
  //   })
  //   result.data.london.slides.forEach(slide => {


  //     createPage({
  //       path: `/slides/${slide.route}`,
  //       component: slideTemplate,
  //       context: {
  //         slide
  //       },
  //     })
  //   })
  // })
//}
