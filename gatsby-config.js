module.exports = {
  siteMetadata: {
    title: `Moscow - Cassiopeia`,
    description: `interactive quest "Moscow - Cassiopeia" in Bett show 2019`,
    author: `CORE`,
    siteUrl: 'https://bett.go2019.ru/'
  },
  plugins: [
    {
      resolve: `gatsby-plugin-polyfill-io`,
      options: {
        features: [`Array.prototype.map`,
                   `Array.prototype.from`]
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Core Quest`,
        short_name: `Quest`,
        start_url: `/`,
        background_color: `#000000`,
        theme_color: `#ffffff`,
        // Enables "Add to Homescreen" prompt and disables browser UI (including back button)
        // see https://developers.google.com/web/fundamentals/web-app-manifest/#display
        display: `standalone`,
        icon: `src/static/icon.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-offline`,
    // `gatsby-plugin-remove-serviceworker`, //TODO if remove offline support
    `gatsby-plugin-react-helmet`,

    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `assets`,
        path: `${__dirname}/src/assets`,
      },
    },
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-yandex-metrika`,
      options: {
        trackingId: '52025617',
        // webvisor: true,
        // trackHash: true,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
      },
    },
    // {
    //   resolve: `gatsby-plugin-google-tagmanager`,
    //   options: {
    //     id: "UA-131027554-3",
    //     includeInDevelopment: false,
    //     // gtmAuth: "YOUR_GOOGLE_TAGMANAGER_ENVIROMENT_AUTH_STRING",
    //     // gtmPreview: "YOUR_GOOGLE_TAGMANAGER_ENVIROMENT_PREVIEW_NAME",
    //   },
    // },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-131027554-3",
        head: false,
      },
    },
    // {
    //   resolve: `gatsby-plugin-google-analytics`,
    //   options: {
    //     trackingId: "",
    //     head: false,
    //     anonymize: true,
    //     respectDNT: true,
    //     exclude: ["/preview/**", "/do-not-track/me/too/"],
    //     optimizeId: "YOUR_GOOGLE_OPTIMIZE_TRACKING_ID",
    //     experimentId: "YOUR_GOOGLE_EXPERIMENT_ID",
    //     variationId: "YOUR_GOOGLE_OPTIMIZE_VARIATION_ID",
    //     sampleRate: 5,
    //     siteSpeedSampleRate: 10,
    //     cookieDomain: "example.com",
    //   },
    // },
    // `gatsby-plugin-sharp`,
    // {
    //   resolve: `gatsby-plugin-manifest`,
    //   options: {
    //     name: `gatsby-starter-default`,
    //     short_name: `starter`,
    //     start_url: `/`,
    //     background_color: `#000`,
    //     theme_color: `#fff`,
    //     display: `minimal-ui`,
    //     icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
    //   },
    // },
    `gatsby-mdx`,
    {
      resolve: `gatsby-mdx`,
      options: {
        extensions: [".mdx", ".md"],
        defaultLayouts: {
          default: require.resolve("./src/components/slide.js")
        },
        mdPlugins: [],
        gatsbyRemarkPlugins: [
          {
            resolve: "gatsby-remark-images",
            options: {
              maxWidth: 1035,
              sizeByPixelDensity: true
            }
          }
        ]
      }
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "slides",
        path: `${__dirname}/src/slides/`
      }
    },
    {
      resolve: "gatsby-source-graphql",
      options: {
        typeName: "SLIDES",
        fieldName: "london",
        url: "https://api-euwest.graphcms.com/v1/cjqzphkrm439001hq3brcbtv3/master",
      },
    },
    {
      resolve: `gatsby-plugin-layout`,
      options: {
        component: require.resolve(`./src/components/layout.js`)
      }
    },
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {
      },
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /assets/
        }
      }
    },
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        // host: '....',
        // sitemap: '....xml',
        policy: [{ userAgent: '*', allow: '/' }]
      }
    },
    `gatsby-plugin-sitemap`,
    'gatsby-plugin-root-import'

    // {
    //   resolve: `gatsby-plugin-postcss`,
    //   options: {
    //     postCssPlugins: [require(`postcss-preset-env`)({ stage: 0 })],
    //   },
    // },

    // {
    //   resolve: `gatsby-source-graphcms`,
    //   options: {
    //     endpoint: `https://api-euwest.graphcms.com/v1/cjqzphkrm439001hq3brcbtv3/master`,
    //     query: `
    //       slides {
    //         header1
    //         header2
    //         text
    //       }
    //     `
    //   }
    // },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.app/offline
    // 'gatsby-plugin-offline',
  ],
}
