/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

//Remove if use polyfill.io
exports.onClientEntry = () => {
  require('core-js/fn/array/from');
};
